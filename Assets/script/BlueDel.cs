﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BlueDel : MonoBehaviour {
	
	static float score;
	int clicked =0;
	// Use this for initialization
	void Start () {
		score = 0;
	}

	// Update is called once per frame
	void Update () {

		//Debug.Log ("点数は"+score);
		transform.Rotate(4, -2, 10);
		if (clicked == 1) {
			transform.Rotate (-100, -50, 100);
			//クリックされたら派手に回転
		}

	}


	void OnMouseDown(){
		if (gameObject.transform.position.y > -2.0f&&gameObject.transform.position.y < 1.6f) {
			GetComponent<AudioSource> ().Play ();
			GetComponent < CircleCollider2D>().enabled= false;
			score = score + 8;
			clicked =1;
			Invoke ("delete", 0.2f);
			name="clicked";

		}
	}

	void delete(){
		GetComponent<ParticleSystem> ().Play ();
		GetComponent<SpriteRenderer> ().color = new Color (1f, 1.0f, 1.0f, 0f);
	} //クリックされたら回転用の関数


	public static float Scorepoint() {
		return score;
	}




}
