﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class RingMaking : MonoBehaviour {

	public AudioClip se;
	public AudioClip clear;
	public AudioClip damage;

	int one=0;
	int damageC=0;
	int damageC2=0;
	GameObject redringprefab;
	GameObject blueringprefab;
	GameObject yellowringprefab;
	GameObject redbossringprefab;
	GameObject bluebossringprefab;
	GameObject yellowbossringprefab;
	GameObject[] obj = new GameObject[22];
	GameObject[] obj2 = new GameObject[22];
	GameObject[] obj3 = new GameObject[22];
	GameObject[] obj4 = new GameObject[14];
	GameObject[] obj5 = new GameObject[14];
	GameObject[] obj6 = new GameObject[14];
	Vector3[] tmp = new Vector3[22];
	GameObject[] tagObjects;
	public GameObject gameover;
	public GameObject gameclear;
	public GameObject life3;
	public GameObject life2;
	public GameObject life1;
	public GameObject life0;

	float start =10;
	public Sprite OutRing;
	public Sprite LightBg;
	float x=0;
	float x2=0;
	float x3=0;


	float gameoversize=0;
	float gameclearalpha=0;


	float sumTime = 2f;
	float duration = 1.5f;
	float length = 0.1f;





	// Use this for initialization
	void Start () {



		redringprefab = GameObject.Find ("redring");
		blueringprefab = GameObject.Find ("bluering");
		yellowringprefab = GameObject.Find ("yellowring");
		redbossringprefab = GameObject.Find ("redbossring");
		bluebossringprefab = GameObject.Find ("bluebossring");
		yellowbossringprefab = GameObject.Find ("yellowbossring");
		gameover.GetComponent<SpriteRenderer> ().color = new Color (1f, 1.0f, 1.0f, 0f);
		gameover.GetComponent < BoxCollider2D>().enabled= false;
		gameclear.GetComponent<SpriteRenderer> ().color = new Color (1f, 1.0f, 1.0f, 0f);

		life2.GetComponent<SpriteRenderer> ().color = new Color (1f, 1.0f, 1.0f, 0f);
		life1.GetComponent<SpriteRenderer> ().color = new Color (1f, 1.0f, 1.0f, 0f);
		life0.GetComponent<SpriteRenderer> ().color = new Color (1f, 1.0f, 1.0f, 0f);
		//ライフの透明化

		for (int i = 0; i <= 21; i++) {
			obj [i] = Instantiate (redringprefab, new Vector3 (i+100, 3, 0), Quaternion.identity) as GameObject;
			obj[i].name = "redringprefab"+i;
			} //赤のリングを複製 親元のredringはいろいろ除外されることに注意

		for (int i2 = 0; i2<= 21; i2++) {
			obj2 [i2] = Instantiate (blueringprefab, new Vector3 (i2+100, 3, 0), Quaternion.identity) as GameObject;
			obj2[i2].name = "blueringprefab"+i2;
		} //青のリングを複製 親元のredringはいろいろ除外されることに注意


		for (int i3 = 0; i3<= 21; i3++) {
			obj3 [i3] = Instantiate (yellowringprefab, new Vector3 (i3+100, 3, 0), Quaternion.identity) as GameObject;
			obj3[i3].name = "yellowringprefab"+i3;
		} //黄のリングを複製 親元のredringはいろいろ除外されることに注意

		for (int i4 = 0; i4<= 13; i4++) {
			obj4 [i4] = Instantiate (redbossringprefab, new Vector3 (i4+100, 3, 0), Quaternion.identity) as GameObject;
			obj4[i4].name = "redbossringprefab"+i4;
		} //ボス赤のリングを複製

		for (int i5 = 0; i5<= 13; i5++) {
			obj5 [i5] = Instantiate (bluebossringprefab, new Vector3 (i5+100, 3, 0), Quaternion.identity) as GameObject;
			obj5[i5].name = "bluebossringprefab"+i5;
		} //ボス青のリングを複製

		for (int i6 = 0; i6<= 13; i6++) {
			obj6 [i6] = Instantiate (yellowbossringprefab, new Vector3 (i6+100, 3, 0), Quaternion.identity) as GameObject;
			obj6[i6].name = "yellowbossringprefab"+i6;
		} //ボス黄のリングを複製


		for (int m = 0; m <= 2; m++) {
			

			float xD = Random.Range (0, 7);

			if (xD >= 0 && xD < 1) {
				x = 0;
				x2 = 6f;
				x3 = -6f;
			}else if (xD >= 1 && xD < 2) {
				x = 6f;
				x2 = 0;
				x3 = -6f;
			}else if(xD >= 2 && xD < 3) {
				x = 6f;
				x2 = -6f;
				x3 = 0;
			}else if(xD >= 4 && xD < 5) {
				x = -6f;
				x2 = 6f;
				x3 = 0;
			}else if(xD >= 5 && xD < 6) {
				x = -6f;
				x2 = 0;
				x3 = 6f;
			}else{
				x = 0;
				x2 = -6f;
				x3 = 6f;
			}
				
			obj [m].transform.position = new Vector3 (x, start+m*7, 0);
			obj2 [m].transform.position = new Vector3 (x2, start+m*7, 0);
			obj3 [m].transform.position = new Vector3 (x3, start+m*7, 0);

		}//直線落ち
			
		obj [3].transform.position = new Vector3 (-6f, 3*10-2+5, 0);
		obj2 [3].transform.position = new Vector3 (0, 3*10+5, 0);
		obj3 [3].transform.position = new Vector3 (6f, 3*10+1.5f+5, 0);

		obj [4].transform.position = new Vector3 (6f, 4*10-2+5, 0);
		obj2 [4].transform.position = new Vector3 (0, 4*10+5, 0);
		obj3 [4].transform.position = new Vector3 (-6f, 4*10+1.5f+5, 0);

		obj [5].transform.position = new Vector3 (-6f, 5*10-2+5, 0);
		obj2 [5].transform.position = new Vector3 (0, 5*10+5, 0);
		obj3 [5].transform.position = new Vector3 (6f, 5*10+1.5f+5, 0);


		//斜め落ち


		for (int m = 10; m <= 12; m++) {
			
			obj [m].transform.position = new Vector3 (x, 65+m, 0);
			obj2 [m].transform.position = new Vector3 (x2, 69+m, 0);
			obj3 [m].transform.position = new Vector3 (x3,53+m,0);

		}

		for (int m = 13; m <= 15; m++) {
			obj [m].transform.position = new Vector3 (x, 80+m, 0);
			obj2 [m].transform.position = new Vector3 (x2, 85+m, 0);
			obj3 [m].transform.position = new Vector3 (x3, 88+m, 0);

		}

		//連続落ち2つ


		obj [16].transform.position = new Vector3 (-6f, 121, 0);
		obj3 [16].transform.position = new Vector3 (6f, 121, 0);
		obj2 [16].transform.position = new Vector3 (0, 131, 0);
		obj2 [17].transform.position = new Vector3 (0, 132, 0);
		obj [17].transform.position = new Vector3 (6f, 124, 0);
		obj3 [17].transform.position = new Vector3 (-6f, 124, 0);
		//難し落ち1


		obj [6].transform.position = new Vector3 (-6f, 138, 0);
		obj2 [6].transform.position = new Vector3 (6f, 138, 0);
		obj [7].transform.position = new Vector3 (6f, 143, 0);
		obj2 [7].transform.position = new Vector3 (-6f, 143, 0);
		obj3 [6].transform.position = new Vector3 (0, 148, 0);
		obj3 [7].transform.position = new Vector3 (0, 149, 0);
		//難し落ち2


		obj [8].transform.position = new Vector3 (-6f, 157, 0);
		obj3 [8].transform.position = new Vector3 (6f, 157, 0);
		obj2 [8].transform.position = new Vector3 (0, 167, 0);
		obj2 [9].transform.position = new Vector3 (0, 168, 0);
		obj [9].transform.position = new Vector3 (6f, 160, 0);
		obj3 [9].transform.position = new Vector3 (-6f, 160, 0);
		//難し落ち3
	


		obj [18].transform.position = new Vector3 (6f, 177, 0);
		obj2 [18].transform.position = new Vector3 (0, 175, 0);
		obj3 [18].transform.position = new Vector3 (-6f, 173, 0);

		obj [19].transform.position = new Vector3 (0, 185, 0);
		obj2 [19].transform.position = new Vector3 (6f, 183, 0);
		obj3 [19].transform.position = new Vector3 (-6f, 187, 0);

		obj [20].transform.position = new Vector3 (-6f, 193, 0);
		obj2 [20].transform.position = new Vector3 (6f, 197, 0);
		obj3 [20].transform.position = new Vector3 (0, 195, 0);

		//斜め落ち２




		obj [21].transform.position = new Vector3 (6f, 203, 0);
		obj2 [21].transform.position = new Vector3 (0, 203, 0);
		obj3 [21].transform.position = new Vector3 (-6f, 203, 0);
		//ラストの直線落ち


		for (int m = 0; m <= 2; m++) {


			float xD = Random.Range (0, 7);

			if (xD >= 0 && xD < 1) {
				x = 0;
				x2 = 6f;
				x3 = -6f;
			}else if (xD >= 1 && xD < 2) {
				x = 6f;
				x2 = 0;
				x3 = -6f;
			}else if(xD >= 2 && xD < 3) {
				x = 6f;
				x2 = -6f;
				x3 = 0;
			}else if(xD >= 4 && xD < 5) {
				x = -6f;
				x2 = 6f;
				x3 = 0;
			}else if(xD >= 5 && xD < 6) {
				x = -6f;
				x2 = 0;
				x3 = 6f;
			}else{
				x = 0;
				x2 = -6f;
				x3 = 6f;
			}

			obj4 [m].transform.position = new Vector3 (x, 213+m*6, 0);
			obj5 [m].transform.position = new Vector3 (x2,213+m*6, 0);
			obj6 [m].transform.position = new Vector3 (x3, 213+m*6, 0);
		}//ボスの直線落ち


		obj4 [3].transform.position = new Vector3 (-6f,233, 0);
		obj5 [3].transform.position = new Vector3 (0, 235, 0);
		obj6 [3].transform.position = new Vector3 (6f, 237, 0);

		obj4 [4].transform.position = new Vector3 (6f, 243, 0);
		obj5 [4].transform.position = new Vector3 (0, 245, 0);
		obj6 [4].transform.position = new Vector3 (-6f, 247, 0);

		obj4 [5].transform.position = new Vector3 (-6f, 253, 0);
		obj5 [5].transform.position = new Vector3 (0,255, 0);
		obj6 [5].transform.position = new Vector3 (6f,257, 0);


		//ボスの斜め落ち


		obj4 [6].transform.position = new Vector3 (-6f, 272, 0);
		obj6 [6].transform.position = new Vector3 (6f, 272, 0);
		obj5 [6].transform.position = new Vector3 (0, 282, 0);
		obj5 [7].transform.position = new Vector3 (0, 283, 0);
		obj4 [7].transform.position = new Vector3 (6f, 275, 0);
		obj6 [7].transform.position = new Vector3 (-6f, 275, 0);
		//難し落ち

		obj4 [8].transform.position = new Vector3 (-6f, 290, 0);
		obj5 [8].transform.position = new Vector3 (6f, 290, 0);
		obj4 [9].transform.position = new Vector3 (6f, 295, 0);
		obj5 [9].transform.position = new Vector3 (-6f, 295, 0);
		obj6 [8].transform.position = new Vector3 (0, 300, 0);
		obj6 [9].transform.position = new Vector3 (0, 301, 0);


		//難し落ち


		obj4 [10].transform.position = new Vector3 (6f, 310, 0);
		obj5 [10].transform.position = new Vector3 (0, 308, 0);
		obj6 [10].transform.position = new Vector3 (-6f, 306, 0);

		obj4 [11].transform.position = new Vector3 (0, 322, 0);
		obj5 [11].transform.position = new Vector3 (6f, 320, 0);
		obj6 [11].transform.position = new Vector3 (-6f, 324, 0);

		obj4 [12].transform.position = new Vector3 (-6f, 334, 0);
		obj5 [12].transform.position = new Vector3 (6f, 338, 0);
		obj6 [12].transform.position = new Vector3 (0, 336, 0);

		obj4 [13].transform.position = new Vector3 (-6f, 346, 0);
		obj5 [13].transform.position = new Vector3 (6f, 346, 0);
		obj6 [13].transform.position = new Vector3 (0, 346, 0);


		for (int j1 = 0; j1 <= 21; j1++) {
			obj[j1].transform.GetComponent<Rigidbody2D> ().velocity = new Vector3 (0, -6.0f, 0);
			obj2[j1].transform.GetComponent<Rigidbody2D> ().velocity = new Vector3 (0, -6.0f, 0);
			obj3[j1].transform.GetComponent<Rigidbody2D> ().velocity = new Vector3 (0, -6.0f, 0);

		}
		//リングに速度を持たせる



	}




	// Update is called once per frame
	void Update () {












		sumTime += Time.deltaTime;
		for (int j1 = 0; j1 <= 13; j1++) {
			if (obj4 [j1].transform.position.y < 8.5f) {
				obj4 [j1].transform.GetComponent<Rigidbody2D> ().velocity = new Vector3 (0, -7f, 0);
				obj5 [j1].transform.GetComponent<Rigidbody2D> ().velocity = new Vector3 (0, -7f, 0);
				obj6 [j1].transform.GetComponent<Rigidbody2D> ().velocity = new Vector3 (0, -7f, 0);
			} else {
				obj4 [j1].transform.GetComponent<Rigidbody2D> ().velocity = new Vector3 (0, -6.0f, 0);
				obj5 [j1].transform.GetComponent<Rigidbody2D> ().velocity = new Vector3 (0, -6.0f, 0);
				obj6 [j1].transform.GetComponent<Rigidbody2D> ().velocity = new Vector3 (0, -6.0f, 0);
			
			}
		} //ボスについては途中で速度を変えるのでアップデート内で速度を制御

		if (obj [21].transform.position.y < -5) {
			GameObject.Find ("background").GetComponent<SpriteRenderer> ().sprite =LightBg;
		}



		for (int t = 0; t <= 21; t++) {
			tmp [t] = obj [t].transform.position;
			//それぞれのオブジェクトの位置を取得

			if (tmp[t].y <= 0.76f&&obj [t].name != "clicked") {
				obj[t].name = "redringout";　
				obj [t].tag = "out";
				//Debug.Log ("out"+t);
				obj[t].GetComponent<SpriteRenderer> ().sprite = OutRing;
				obj[t].GetComponent < CircleCollider2D>().enabled= false;
				obj[t].transform.Rotate (0, 0, 100);
			}　//取得した位置に基づいて一定の位置以下かつ透明じゃなければoutのタグをつける
		}



		for (int t2 = 0; t2 <= 21; t2++) {
			tmp [t2] = obj2 [t2].transform.position;
			//それぞれのオブジェクトの位置を取得

			if (tmp[t2].y <= -2.0f&&obj2 [t2].name != "clicked") {
				obj2[t2].name = "blueringout";　
				obj2 [t2].tag = "out";
				//Debug.Log ("out"+t2);
				obj2[t2].GetComponent<SpriteRenderer> ().sprite = OutRing;
				obj2[t2].transform.Rotate (0, 0, 100);
			}　//取得した位置に基づいて一定の位置以下かつ透明じゃなければoutのタグをつける
		}


		for (int t3 = 0; t3 <= 21; t3++) {
			tmp [t3] = obj3 [t3].transform.position;
			//それぞれのオブジェクトの位置を取得

			if(tmp[t3].y<-5.7f&&obj3[t3].name!= "clicked"){
				obj3[t3].name = "yellowringout";　
				obj3[t3].tag = "out";
				//Debug.Log ("out"+t3);
				obj3[t3].GetComponent<SpriteRenderer> ().sprite = OutRing;
				obj3[t3].transform.Rotate (0, 0, 100);
			//取得した位置に基づいて一定の位置以下かつ透明じゃなければoutのタグをつける
			}

		}


		for (int t4 = 0; t4 <= 13; t4++) {
			tmp [t4] = obj4 [t4].transform.position;
			//それぞれのオブジェクトの位置を取得

			if (tmp[t4].y <= 0.76f&&obj4 [t4].name != "clicked") {
				obj4[t4].name = "redringout";　
				obj4 [t4].tag = "out";
				//Debug.Log ("out"+t);
				obj4[t4].GetComponent<SpriteRenderer> ().sprite = OutRing;
				obj4[t4].transform.Rotate (0, 0, 100);
			}　//取得した位置に基づいて一定の位置以下かつ透明じゃなければoutのタグをつける
		}


		for (int t5 = 0; t5 <= 13; t5++) {
			tmp [t5] = obj5 [t5].transform.position;
			//それぞれのオブジェクトの位置を取得

			if (tmp[t5].y <= -2.0f&&obj5 [t5].name != "clicked") {
				obj5[t5].name = "blueringout";　
				obj5 [t5].tag = "out";
				//Debug.Log ("out"+t2);
				obj5[t5].GetComponent<SpriteRenderer> ().sprite = OutRing;
				obj5[t5].transform.Rotate (0, 0, 100);
			}　//取得した位置に基づいて一定の位置以下かつ透明じゃなければoutのタグをつける
		}


		for (int t6 = 0; t6 <= 13; t6++) {
			tmp [t6] = obj6 [t6].transform.position;
			//それぞれのオブジェクトの位置を取得

			if (tmp[t6].y <= -5.7f&&obj6 [t6].name != "clicked") {
				obj6[t6].name = "yellowringout";　
				obj6[t6].tag = "out";
				//Debug.Log ("out"+t3);
				obj6[t6].GetComponent<SpriteRenderer> ().sprite = OutRing;
				obj6[t6].transform.Rotate (0, 0, 100);
			}　//取得した位置に基づいて一定の位置以下かつ透明じゃなければoutのタグをつける


		}





		tagObjects = GameObject.FindGameObjectsWithTag ("out");



		switch (tagObjects.Length) {
		case 1:
			if (damageC == 0) {
				GetComponent<AudioSource> ().PlayOneShot (damage, 1f);
				Handheld.Vibrate();
				damageC = 1; //一回のみ鳴らす
			}
			life3.GetComponent<SpriteRenderer> ().color = new Color (1f, 1.0f, 1.0f, 0f);
			life2.GetComponent<SpriteRenderer> ().color = new Color (1f, 1.0f, 1.0f, 1f);

			break;
		case 2:
			if (damageC2 == 0) {
				GetComponent<AudioSource> ().PlayOneShot (damage, 1f);
				Handheld.Vibrate();
				damageC2 = 1;
			}
			life2.GetComponent<SpriteRenderer> ().color = new Color (1f, 1.0f, 1.0f, 0f);
			life1.GetComponent<SpriteRenderer> ().color = new Color (1f, 1.0f, 1.0f, 1f);
			break;
		case 3:
			
			if (one ==0) {
				GetComponent<AudioSource> ().PlayOneShot (se,2f);
				Handheld.Vibrate();
				one = 1;
			}



			life1.GetComponent<SpriteRenderer> ().color = new Color (1f, 1.0f, 1.0f, 0f);
			life0.GetComponent<SpriteRenderer> ().color = new Color (1f, 1.0f, 1.0f, 1f);
			Debug.Log ("3");
			Invoke ("Score", 2.5f);
			GameObject.Find ("background").transform.Rotate (-100, -10, 100);
			//全部がぐちゃぐちゃになる
			GameObject.Find ("gameoverparticle").GetComponent<ParticleSystem> ().Play ();
			gameover.GetComponent<SpriteRenderer> ().color = new Color (0.7f, 0.7f, 0.7f, 0.7f);
			gameover.GetComponent < BoxCollider2D>().enabled= true;
			gameover.transform.Rotate (0, 0, 0); 
			break;
		default:
			break;
		}
		//outの数で判定



		if (gameover.GetComponent<SpriteRenderer> ().color.a == 0.7f) {
			
				gameoversize = gameoversize + 0.1f;
				gameover.transform.localScale = new Vector3 (gameoversize+1.3f, gameoversize, gameoversize);
			if (gameoversize >= 1.1f) {
				gameoversize =1.1f;}
			//ゲームおーばの文字を徐々に大きくする


			gameover.transform.position = new Vector3 (gameover.transform.position.x, Mathf.PingPong (sumTime * length / duration * 20, length), gameover.transform.position.z);




		}

	

		if (obj6 [13].transform.position.y <-5) {
			GameObject.Find ("gameclearparticle").GetComponent<ParticleSystem> ().Play ();
			GetComponent<AudioSource> ().PlayOneShot (clear,0.5f);
			gameclear.transform.localScale = new Vector3 (0.3f, 0.7f, 1);

		}

		if (gameclear.transform.localScale.z == 1) {
			gameclearalpha = gameclearalpha + 0.01f;
			gameclear.GetComponent<SpriteRenderer> ().color = new Color (1, 1, 1, gameclearalpha);

			if (gameclearalpha >= 1f) {
				gameclearalpha = 1f;
			}
			Invoke ("Score", 5f);

		}//ゲームクリアの文字を徐々に濃くする





	}































	void Score(){SceneManager.LoadScene ("Score");}

		
}
