﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class YellowDel : MonoBehaviour {
	private AudioSource sound01;
	static float score;
	int clicked =0;
	// Use this for initialization
	void Start () {
		sound01 = GetComponent<AudioSource> ();
		score = 0;
	}

	// Update is called once per frame
	void Update () {

		//Debug.Log ("点数は"+score);
		transform.Rotate(3, 2, -10);
		if (clicked == 1) {
			transform.Rotate (-100, -80, 100);
			//クリックされたら派手に回転
		}


	}


	void OnMouseDown(){
		if (gameObject.transform.position.y > -5.7f&&gameObject.transform.position.y<-1.3f) {
			GetComponent < CircleCollider2D>().enabled= false; //これをしないとほかのやつに被ったときに邪魔になる
			sound01.PlayOneShot (sound01.clip);
			score = score + 3;
			clicked =1;
			Invoke ("delete", 0.2f);
			name="clicked";


		}
	}

	void delete(){
		GetComponent<ParticleSystem> ().Play ();
		GetComponent<SpriteRenderer> ().color = new Color (1f, 1.0f, 1.0f, 0f);
	} //クリックされたら回転用の関数


	public static float Scorepoint() {
		return score;
	}




}
